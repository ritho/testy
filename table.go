package testy

import (
	"fmt"
	"reflect"
	"testing"
)

// generator should return a test, and an optional cleanup func.
type generator func(*testing.T) (interface{}, finalFn)

// A Final can be used to do any final checks, clean up after an
// individual test, or a complete table of tests. It must be a function
// with one of the following signatures:
//
//   - func()
//   - func(*testing.T)
//   - func() error
//
// In the latter case, any non-nil error will be passed to t.Fatal().
type Final interface{}

type finalFn func(*testing.T)

type finalFns []func(*testing.T)

func (f finalFns) run(t *testing.T) {
	for _, fn := range f {
		fn(t)
	}
}

// Table is a table of one or more tests to run against a single test function.
type Table struct {
	gens    map[string]generator
	cleanup []finalFn
}

// NewTable returns a new Table instance.
func NewTable() *Table {
	return &Table{
		gens:    make(map[string]generator),
		cleanup: make([]finalFn, 0),
	}
}

// Add adds a single named test to the table, and optionally one or more
// cleanup functions. The cleanup function may also be returned by the test
// generator, if that is easier for scoping of closures.
//
// test may be of the following types:
//
//   - interface{}
//   - func() interface{}
//   - func(*testing.T) interface{}
//   - func() (interface{}, CleanupFunc)
//   - func(*testing.T) (interface{}, CleanupFunc)
//
// If multiple cleanup functions are provided, first the one returned by the
// generator is executed, then any passed to Add() are executed, in the order
// provided.
func (tb *Table) Add(name string, test interface{}, final ...Final) {
	if _, ok := tb.gens[name]; ok {
		panic("Add(): Test " + name + " already defined.")
	}
	cleanFuncs, err := prepareCleanup(final...)
	if err != nil {
		panic(err)
	}
	var gen generator
	switch typedTest := test.(type) {
	case func() interface{}:
		gen = func(_ *testing.T) (interface{}, finalFn) {
			return typedTest(), cleanFuncs.run
		}
	case func(*testing.T) interface{}:
		gen = func(t *testing.T) (interface{}, finalFn) {
			return typedTest(t), cleanFuncs.run
		}
	case func() (interface{}, Final):
		gen = func(_ *testing.T) (interface{}, finalFn) {
			test, clean := typedTest()
			cleanup, err := prepareCleanup(clean)
			if err != nil {
				panic(err)
			}

			if len(cleanup) > 0 {
				cleanup = append(cleanup, cleanFuncs...)
			}

			return test, cleanup.run
		}
	case func(*testing.T) (interface{}, Final):
		gen = func(t *testing.T) (interface{}, finalFn) {
			test, clean := typedTest(t)
			cleanup, err := prepareCleanup(clean)
			if err != nil {
				panic(err)
			}

			if len(cleanup) > 0 {
				cleanup = append(cleanup, cleanFuncs...)
			}

			return test, cleanup.run
		}
	default:
		if reflect.TypeOf(test).Kind() == reflect.Func {
			panic(fmt.Sprintf("Test generator must be of type func(*testing.T) "+
				"interface{}, or func() interface{}. Got %T", test))
		}

		gen = func(_ *testing.T) (interface{}, finalFn) {
			return test, cleanFuncs.run
		}
	}

	tb.gens[name] = gen
}

// prepareCleanups takes 0 or more functions, and returns a single CleanupFunc,
// or returns an error if there was a type assertion failure.
func prepareCleanup(funcs ...Final) (finalFns, error) {
	if len(funcs) == 0 {
		return nil, nil
	}

	fns := make([]func(*testing.T), 0, len(funcs))
	for _, fn := range funcs {
		switch typedFn := fn.(type) {
		case func():
			fns = append(fns, func(_ *testing.T) {
				typedFn()
			})
		case func() error:
			fns = append(fns, func(t *testing.T) {
				if err := typedFn(); err != nil {
					t.Fatal(err)
				}
			})
		case func(*testing.T):
			fns = append(fns, typedFn)
		default:
			return nil, fmt.Errorf("Cleanup func must be of type func(), func(*testing.T), or func() error. Got %T", fn)
		}
	}

	return fns, nil
}

// Cleanup takes a single function to be called after all Table tests have been
// executed.
func (tb *Table) Cleanup(fn Final) {
	var cleanup finalFn
	switch typedFn := fn.(type) {
	case func():
		cleanup = func(_ *testing.T) { typedFn() }
	case func(*testing.T):
		cleanup = typedFn
	case func() error:
		cleanup = func(t *testing.T) {
			t.Helper()
			if e := typedFn(); e != nil {
				t.Error(e)
			}
		}
	default:
		panic("Cleanup function must be func(), func() error, or func(*testing.T)")
	}

	tb.cleanup = append(tb.cleanup, cleanup)
}

func (tb *Table) doCleanup(t *testing.T) {
	for _, fn := range tb.cleanup {
		fn(t)
	}
}

// Run cycles through the defined tests, passing them one at a time to testFn.
// testFn must be a function which takes two arguments: *testing.T, and an
// arbitrary type, which must match the return value of the Generator functions.
func (tb *Table) Run(t *testing.T, testFn interface{}) {
	defer tb.doCleanup(t)
	testFnT := reflect.TypeOf(testFn)
	if testFnT.Kind() != reflect.Func {
		panic("testFn must be a function")
	}

	if testFnT.NumIn() != 2 || testFnT.In(0) != reflect.TypeOf(&testing.T{}) {
		panic("testFn must be of the form func(*testing.T, **)")
	}

	testType := reflect.TypeOf(testFn).In(1)
	testFnV := reflect.ValueOf(testFn)
	for name, genFn := range tb.gens {
		t.Run(name, func(t *testing.T) {
			t.Helper()
			test, cleanup := genFn(t)
			if cleanup != nil {
				defer cleanup(t)
			}

			if reflect.TypeOf(test) != testType {
				t.Fatalf("Test generator returned wrong type. Have %T, want %s", test, testType.Name())
			}

			_ = testFnV.Call([]reflect.Value{reflect.ValueOf(t), reflect.ValueOf(test)})
		})
	}
}
