package testy

import (
	"runtime"
	"strings"
	"testing"

	"github.com/Masterminds/semver/v3"
)

// SkipVer skips the test if the current version of Go matches expr. See
// https://pkg.go.dev/github.com/Masterminds/semver/v3#hdr-Basic_Comparisons
// for an explanation of the supported format of expr.
//
// To determine the current version of Go, runtime.Version() is called, the
// 'go' prefix is stripped, and if an rc version, a '-' character is inserted
// before the 'rc' substring, to make the version conform to semver.
func SkipVer(t *testing.T, expr string) {
	t.Helper()
	verCon, err := semver.NewConstraint(expr)
	if err != nil {
		t.Fatalf("Invalid semver constraint %s: %s", expr, err)
	}
	gov, _ := semver.NewVersion(goVer)
	if verCon.Check(gov) {
		t.Skipf("Skipping test, Go %s matches version constraint %s", runtime.Version(), expr)
	}
}

var goVer = strings.ReplaceAll(
	strings.TrimPrefix(runtime.Version(), "go"),
	"rc", "-rc")
