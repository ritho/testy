package testy

import (
	"fmt"
	"net/url"
)

// DiffURLQuery unmarshals two url query strings (can also be
// application/x-www-form-urlencoded bodies), then calls DiffInterface on them.
func DiffURLQuery(expected, actual interface{}, re ...Replacement) *Diff {
	expectedQuery, err := toText(expected)
	if err != nil {
		return &Diff{err: fmt.Sprintf("expected: %s", err)}
	}
	actualQuery, err := toText(actual)
	if err != nil {
		return &Diff{err: fmt.Sprintf("actual: %s", err)}
	}
	var expectedValues, actualValues url.Values

	if expectedValues, err = url.ParseQuery(expectedQuery); err != nil {
		return &Diff{err: fmt.Sprintf("failed to parse expected query: %s", err)}
	}
	if actualValues, err = url.ParseQuery(actualQuery); err != nil {
		return &Diff{err: fmt.Sprintf("failed to parse actual query: %s", err)}
	}
	d := DiffInterface(expectedValues, actualValues, re...)
	return update(UpdateMode(), expected, actualQuery, d)
}
