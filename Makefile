.PHONY: tests
tests:
	@go test ./... -race

.PHONY: update-lint
update-lint:
	@curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $$(go env GOPATH)/bin latest

.PHONY: lint
lint:
	@golangci-lint run -c $(PWD)/.golangci.toml ./...
