package testy

import (
	"errors"
	"testing"
)

func TestErrorMatches(t *testing.T) {
	type tt struct {
		want   string
		got    error
		result bool
	}

	tests := NewTable()

	tests.Add("want no error", tt{
		want:   "",
		got:    nil,
		result: true,
	})
	tests.Add("unwanted error", tt{
		want:   "",
		got:    errors.New("foo"),
		result: false,
	})
	tests.Add("missing error", tt{
		want:   "foo",
		got:    nil,
		result: false,
	})
	tests.Add("expected error", tt{
		want:   "foo",
		got:    errors.New("foo"),
		result: true,
	})
	tests.Add("unexpected error", tt{
		want:   "foo",
		got:    errors.New("bar"),
		result: false,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := ErrorMatches(tt.want, tt.got)
		if result != tt.result {
			t.Errorf("Got %t, want %t", result, tt.result)
		}
	})
}

func TestErrorMatchesRE(t *testing.T) {
	type tt struct {
		want   string
		got    error
		result bool
	}

	tests := NewTable()

	tests.Add("want no error", tt{
		want:   "",
		got:    nil,
		result: true,
	})
	tests.Add("unwanted error", tt{
		want:   "^$",
		got:    errors.New("foo"),
		result: false,
	})
	tests.Add("missing error", tt{
		want:   "foo",
		got:    nil,
		result: false,
	})
	tests.Add("expected error", tt{
		want:   "foo",
		got:    errors.New("foo"),
		result: true,
	})
	tests.Add("unexpected error", tt{
		want:   "foo",
		got:    errors.New("bar"),
		result: false,
	})
	tests.Add("empty stream means no error", tt{
		want:   "",
		got:    errors.New("foo"),
		result: false,
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		result := ErrorMatchesRE(tt.want, tt.got)
		if result != tt.result {
			t.Errorf("Got %t, want %t", result, tt.result)
		}
	})
}
