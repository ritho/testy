package testy

import (
	"regexp"
	"testing"
)

func TestSwarnf(t *testing.T) {
	tests := []struct {
		name     string
		format   string
		args     []interface{}
		expected string
	}{
		{
			name:     "simple",
			format:   "foo %s",
			args:     []interface{}{"bar"},
			expected: `^\[\d+/\d+\ .*/testy/warn_test.go:\d+] foo bar$`,
		},
		{
			name:     "empty",
			format:   "",
			args:     nil,
			expected: `^\[\d+/\d+\ .*/testy/warn_test.go:\d+] Something's wrong$`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := Swarnf(test.format, test.args...)
			if !regexp.MustCompile(test.expected).MatchString(result) {
				t.Errorf("Unexpected result: %s\nExpected: /%s/\n", result, test.expected)
			}
		})
	}
}

func TestSwarn(t *testing.T) {
	tests := []struct {
		name     string
		args     []interface{}
		expected string
	}{
		{
			name:     "simple",
			args:     []interface{}{"bar"},
			expected: `^\[\d+/\d+\ .*/testy/warn_test.go:\d+] bar$`,
		},
		{
			name:     "empty",
			args:     nil,
			expected: `^\[\d+/\d+\ .*/testy/warn_test.go:\d+] Something's wrong$`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := Swarn(test.args...)
			if !regexp.MustCompile(test.expected).MatchString(result) {
				t.Errorf("Unexpected result: %s\nExpected: /%s/\n", result, test.expected)
			}
		})
	}
}

func TestSwarnln(t *testing.T) {
	tests := []struct {
		name     string
		args     []interface{}
		expected string
	}{
		{
			name:     "simple",
			args:     []interface{}{"bar"},
			expected: `^\[\d+/\d+\ .*/testy/warn_test.go:\d+] bar\n$`,
		},
		{
			name:     "empty",
			args:     nil,
			expected: `^\[\d+/\d+\ .*/testy/warn_test.go:\d+] Something's wrong\n$`,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			result := Swarnln(test.args...)
			if !regexp.MustCompile(test.expected).MatchString(result) {
				t.Errorf("Unexpected result: %s\nExpected: /%s/\n", result, test.expected)
			}
		})
	}
}
