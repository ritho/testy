package testy

import (
	"io"
	"strings"
	"time"
)

type delayReader struct {
	delay time.Duration
}

var _ io.ReadCloser = &delayReader{}

func (r *delayReader) Read(_ []byte) (int, error) {
	time.Sleep(r.delay)
	return 0, io.EOF
}

func (r *delayReader) Close() error { return nil }

// DelayReader returns an io.Reader that never returns any data, but will
// delay before return io.EOF. It is intended to be used in conjunction with
// io.MultiReader to construct test scenarios. Each call to Read() will delay,
// then return (0, io.EOF), so it is safe to re-use the return value. A call
// to Close() will always return nil.
func DelayReader(delay time.Duration) io.ReadCloser {
	return &delayReader{delay: delay}
}

type errReader struct {
	r   io.Reader
	err error
}

var _ io.ReadCloser = &errReader{}

func (r *errReader) Read(p []byte) (int, error) {
	c, err := r.r.Read(p)
	if err == io.EOF {
		err = r.err
	}
	return c, err
}

func (r *errReader) Close() error { return r.err }

// ErrorReader returns an io.Reader which behaves the same as
// strings.NewReader(s), except that err will be returned at the end, rather
// than io.EOF. A call to Close() will also return err.
func ErrorReader(s string, err error) io.ReadCloser {
	return &errReader{r: strings.NewReader(s), err: err}
}

type neverReader struct{}

var _ io.Reader = &neverReader{}

func (neverReader) Read([]byte) (int, error) {
	select {}
}

// NeverReader returns an io.Reader that never returns data, blocking forever
// when Read() is called.
func NeverReader() io.Reader {
	return &neverReader{}
}
