package testy

import (
	"testing"
)

func TestTableTests(t *testing.T) {
	addFunc := func(a, b int) int {
		return a + b
	}
	type ttTest struct {
		a, b   int
		output int
	}
	table := NewTable()
	table.Add("one", func(_ *testing.T) interface{} {
		return ttTest{a: 1, output: 1}
	})
	table.Add("two", ttTest{a: 1, b: 1, output: 2})
	table.Add("three", func() interface{} {
		return ttTest{a: 1, b: 2, output: 3}
	})
	table.Run(t, func(t *testing.T, test ttTest) {
		output := addFunc(test.a, test.b)
		if output != test.output {
			t.Errorf("Expected %d, got %d\n", test.output, output)
		}
	})

	t.Run("cleanup", func(t *testing.T) {
		want := map[int]struct{}{
			1: {}, 2: {}, 3: {},
			4: {}, 5: {}, 6: {},
			7: {}, 8: {},
		}
		got := make(map[int]struct{})
		tbl := NewTable()
		tbl.Add("func()", 0, func() {
			got[1] = struct{}{}
		})
		tbl.Add("func() error", 0, func() error {
			got[2] = struct{}{}
			return nil
		})
		tbl.Add("func(*testing.T)", 0, func(_ *testing.T) {
			got[3] = struct{}{}
		})
		tbl.Add("many", 0, func() {
			got[4] = struct{}{}
		},
			func() error {
				got[5] = struct{}{}
				return nil
			},
			func(_ *testing.T) {
				got[6] = struct{}{}
			},
		)
		tbl.Add("gen func() (interface{}, Final)", func() (interface{}, Final) {
			return 0, func() { got[7] = struct{}{} }
		})
		tbl.Add("gen func(*testing.T) (interface{}, Final)", func(_ *testing.T) (interface{}, Final) {
			return 0, func() { got[8] = struct{}{} }
		})

		tbl.Run(t, func(t *testing.T, _ int) {})
		if d := DiffInterface(want, got); d != nil {
			t.Error(d)
		}
	})
}

func Test_prepareCleanup(t *testing.T) {
	type tt struct {
		fns   []Final
		err   string
		check func(*testing.T)
	}

	tests := NewTable()
	tests.Add("invalid type", tt{
		fns: []Final{123},
		err: "Cleanup func must be of type func(), func(*testing.T), or func() error. Got int",
	})
	tests.Add("func()", func() interface{} {
		var done bool
		return tt{
			fns: []Final{
				func() { done = true },
			},
			check: func(t *testing.T) {
				if !done {
					t.Error("Expected done to be true")
				}
			},
		}
	})
	tests.Add("func() error", func() interface{} {
		var done bool
		return tt{
			fns: []Final{
				func() error { done = true; return nil },
			},
			check: func(t *testing.T) {
				if !done {
					t.Error("Expected done to be true")
				}
			},
		}
	})
	tests.Add("func(*testing.T)", func() interface{} {
		var done bool
		return tt{
			fns: []Final{
				func(_ *testing.T) { done = true },
			},
			check: func(t *testing.T) {
				if !done {
					t.Error("Expected done to be true")
				}
			},
		}
	})
	tests.Add("several", func() interface{} {
		want := map[int]struct{}{
			1: {},
			2: {},
			3: {},
		}
		got := make(map[int]struct{})
		return tt{
			fns: []Final{
				func(_ *testing.T) { got[1] = struct{}{} },
				func() { got[2] = struct{}{} },
				func() { got[3] = struct{}{} },
			},
			check: func(t *testing.T) {
				if d := DiffInterface(want, got); d != nil {
					t.Error(d)
				}
			},
		}
	})

	tests.Run(t, func(t *testing.T, tt tt) {
		got, err := prepareCleanup(tt.fns...)
		Error(t, tt.err, err)
		got.run(t)
		tt.check(t)
	})

	t.Run("no funcs", func(t *testing.T) {
		got, err := prepareCleanup()
		Error(t, "", err)
		if got != nil {
			t.Errorf("Expected nil cleanup func")
		}
	})
}
