package testy

import (
	"fmt"
	"os"
)

const snapshotMode = 0o666

func update(updateMode bool, expected interface{}, actual string, d *Diff) *Diff {
	if d == nil {
		return nil
	}
	expectedFile, ok := expected.(*File)
	if !ok {
		return d
	}
	if !updateMode {
		d.snapshot = true
		return d
	}
	file, err := os.OpenFile(expectedFile.Path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, snapshotMode) // nolint: gas
	if err != nil {
		return &Diff{err: fmt.Sprintf("Update failed: %s", err)}
	}
	defer file.Close() // nolint: errcheck
	if _, e := file.WriteString(actual); e != nil {
		return &Diff{err: fmt.Sprintf("Update failed: %s", e)}
	}
	return nil
}
