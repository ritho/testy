package testy

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"runtime"
	"strconv"
	"strings"
)

const defaultMessage = "Something's wrong"

// Warnln outputs the arguments to STDERR, prefixed with some useful
// debugging info, and suffixed with a newline.
func Warnln(args ...interface{}) {
	fmt.Fprint(os.Stderr, Swarnln(args...))
}

// Swarnln works like Warnln, but returns the string rather than outputting it.
func Swarnln(args ...interface{}) string {
	if len(args) == 0 {
		args = []interface{}{defaultMessage}
	}
	return fmt.Sprintln(append([]interface{}{prefix()}, args...)...)
}

// Warn outputs the arguments to STDERR, prefixed with some useful
// debugging info. The output prefix is in the following format:
//
// [PID/GID file:lineno]
func Warn(args ...interface{}) {
	fmt.Fprint(os.Stderr, Swarn(args...))
}

// Swarn returns a Warn-formatted string.
func Swarn(args ...interface{}) string {
	if len(args) == 0 {
		args = []interface{}{defaultMessage}
	}
	return prefix() + " " + fmt.Sprint(args...)
}

// Swarnf returns a Warn-formatted string.
func Swarnf(format string, args ...interface{}) string {
	if format == "" && len(args) == 0 {
		return fmt.Sprint(prefix(), " ", defaultMessage)
	}
	return fmt.Sprint(prefix(), " ", fmt.Sprintf(format, args...))
}

// Warnf warns to the specified io.Writer.
func Warnf(f io.Writer, format string, args ...interface{}) (int, error) {
	return f.Write([]byte(Swarnf(format, args...)))
}

func prefix() string {
	var file string
	var line int
	for skip := 2; skip < 5; skip++ {
		_, file, line, _ = runtime.Caller(skip)
		if !strings.HasSuffix(file, "gitlab.com/flimzy/testy/warn.go") {
			break
		}
	}
	return fmt.Sprintf("[%d/%d %s:%d]", os.Getpid(), getGID(), file, line)
}

func getGID() uint64 {
	const gidLen = 64
	b := make([]byte, gidLen)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64) // nolint:gomnd
	return n
}
